# .Vault

.Vault to program napisany w języku Python, który wykorzystuje biblioteki tkinter i cryptography do interfejsu użytkownika oraz operacji szyfrowania i deszyfrowania.

## Wprowadzenie

Program umożliwia użytkownikom tworzenie, odczyt i modyfikację skrzynek depozytowych zawierających dane uwierzytelniające. Wykorzystanie plików kluczy do autoryzacji zapewnia wyższy poziom bezpieczeństwa w porównaniu do wpisywania czystego tekstu, eliminując ryzyko ataków typu keylogger. Dodatkowo, program umożliwia automatyczną autoryzację za pomocą pliku `.key` umieszczonego na pendrivie.

## Użycie

Aby uruchomić program `.Vault`, wykonaj następujące kroki:

1. Uruchom terminal lub konsolę.
2. Przejdź do katalogu zawierającego pliki programu `.Vault`.
3. Wykonaj polecenie `python3 main.py`.

### Tworzenie skrzynki depozytowej

Aby utworzyć nową skrzynkę depozytową, wykonaj następujące kroki:

1. Uruchom program `.Vault`, uruchamiając plik `main.py`.
2. Na ekranie logowania kliknij przycisk "create deposit".
3. W nowo otwartym oknie podaj nazwę skrzynki depozytowej.
4. Kliknij przycisk "create", aby utworzyć nowy depozyt.
5. Nowa skrzynka depozytowa zostanie wygenerowana automatycznie przez program, jednocześnie szyfrując ją i generując indywidualny klucz deszyfrujący.

### Logowanie do istniejącej skrzynki depozytowej

Aby zalogować się do istniejącej skrzynki depozytowej, wykonaj następujące kroki:

1. Uruchom program `.Vault`, uruchamiając plik `main.py`.
2. Na ekranie logowania kliknij przycisk "select deposit".
3. Wybierz plik skrzynki depozytowej (.vlt) i kliknij przycisk "load".
4. Podaj klucz dostępu do skrzynki depozytowej, który może być wprowadzony ręcznie lub załadowany z pliku.
5. Kliknij przycisk "authorize", aby uwierzytelnić się i odblokować skrzynkę depozytową.

**Uwaga**: Po wybraniu pliku skrzynki depozytowej, program automatycznie uruchamia procedurę przeszukiwania plików na pendrivie w celu automatycznego wprowadzenia klucza.

## Opis plików

Program `.Vault` składa się z następujących plików:

- `main.py`: Plik główny, który zawiera funkcję `main()` inicjującą program.
- `events/authorization_session.py`: Moduł odpowiedzialny za sesję uwierzytelniania.
- `events/vault_session.py`: Moduł odpowiedzialny za sesję skrzynki depozytowej.
- `libs/keydiger.py`: Moduł zawierający logikę wyszukiwania pliku klucza na podstawie podłączonego pendrive'a.
- `events/add_credentials_session.py`: Moduł odpowiedzialny za sesję dodawania danych uwierzytelniających.
- `events/deposit_creator.py`: Moduł odpowiedzialny za tworzenie nowej skrzynki depozytowej.
- `libs/crypter.py`: Moduł zawierający funkcje do szyfrowania i deszyfrowania skrzynki depozytowej.
- `libs/deposit.py`: Moduł zawierający klasę `Deposit` do obsługi skrzynki depozytowej.
- `libs/tk_builder.py`: Moduł zawierający klasę `TkinterBuilder` do budowania interfejsu użytkownika.
- `libs/menu_bar.py`: Moduł zawierający klasę `MenuBar` do tworzenia paska menu w interfejsie użytkownika.

## Autor

Program `.Vault` został napisany przez `Trotylson`.

## Podziękowania

Chciełbym podziękować wszystkim osobom, które przyczyniły się do rozwoju i testowania programu `.Vault`. Dziękuję za wasze cenne uwagi i opinie, które pomogły mi ulepszyć ten program.
