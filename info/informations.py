from tkinter import messagebox, ttk
from tkinter import *
from libs.tk_builder import TkinterBuilder

class Info():
    def __init__(self):
        pass


    def _show_instructions(self):
        INFO_WINDOW = TkinterBuilder("Informations", "700x700")
        info_frame = Frame(INFO_WINDOW.tkinter)
        with open(f"./info/instructions", 'r') as f:
            file = f.read()
        ttk.Label(info_frame, text="How to use .Vault\n", justify=CENTER, font=('Arial', 10, 'bold')).pack()
        ttk.Label(info_frame, text=file, font=('Arial', 8)).pack()
        info_frame.pack(padx=20, pady=10)
        # messagebox.showinfo('Instructions', file)

    def _show_about(self):
        with open(f"./info/about", 'r') as f:
            file = f.read()
        messagebox.showinfo('About', file)