from tkinter import *
from tkinter import ttk, messagebox
from libs.tk_builder import TkinterBuilder

class AddCredentials():
    def __init__(self, VAULT):
        self.VAULT = VAULT
        self.CREDENTIALS_WINDOW = TkinterBuilder("New credentials", "600x300")
        self.credentials_frame = Frame(self.CREDENTIALS_WINDOW.tkinter, highlightthickness=1)
        self.credentials_frame.config(highlightbackground='black', highlightcolor='blue')
        self.credentials_frame.pack(padx=30, pady=10)

    def create(self):
        
        ttk.Label(self.credentials_frame, text="name: ").grid(row=1, column=1, padx=10)
        name = ttk.Entry(self.credentials_frame)
        name.grid(row=1, column=2, padx=10, pady=5)
        
        ttk.Label(self.credentials_frame, text="login: ").grid(row=2, column=1, padx=10)
        login = ttk.Entry(self.credentials_frame)
        login.grid(row=2, column=2, padx=10, pady=5)
        
        ttk.Label(self.credentials_frame, text="password: ").grid(row=3, column=1, padx=10)
        password = ttk.Entry(self.credentials_frame)
        password.grid(row=3, column=2, padx=10, pady=5)
        
        ttk.Label(self.credentials_frame, text="url: ").grid(row=4, column=1, padx=10)
        url = ttk.Entry(self.credentials_frame)
        url.grid(row=4, column=2, padx=10, pady=5)
        
        ttk.Label(self.credentials_frame, text="description: ").grid(row=5, column=1, padx=10)
        description = ttk.Entry(self.credentials_frame, width=50)
        description.grid(row=5, column=2, padx=10, pady=5)

        def accept_changes():
            print("Accepting changes")
            for row in self.VAULT.deposit_content:
                if row['name'] == name.get():
                    messagebox.showerror("Whait a minute...", f"You used the name {name.get()} with a different row!\nYou can not have two same named rows!\nPlease choose a different name.")
                    return False
            content = {
                'name': name.get(),
                'login': login.get(),
                'password': password.get(),
                'url': url.get(),
                'description': description.get()
            }
            self.VAULT.DEPOSIT.insert_to_deposit(content)
            self.VAULT.event_save_deposit()
            self.VAULT.refresh_credentials_frame()

        ttk.Button(self.credentials_frame, text="Add credentials", command=accept_changes).grid(row=6, column=2, padx=10, pady=20)
        ttk.Button(self.credentials_frame, text="Cancel", command=self.CREDENTIALS_WINDOW.close).grid(row=6, column=3, padx=10, pady=20)