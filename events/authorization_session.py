from libs.tk_builder import TkinterBuilder
from events.deposit_creator import DepositCreator
from tkinter import *
from tkinter import ttk, filedialog, messagebox
import libs.crypter as crypter
from info.informations import Info
import re, threading

class authorization_session():
    def __init__(self, pen_scan_thread):
        """UTILS"""
        self.pendrive_scan_thread = pen_scan_thread

        """Main params"""
        self.authorized = False
        self.key_file_path = None
        self.deposit_path = None
        self.deposit_key = None
        self.deposit_key_name = None
        self.deposit_box = None

        """Create a new login screen"""
        self.login_window = TkinterBuilder(title=".Vault login", window_size="550x270")
        # self.login_window.config()
        
        self.frame = Frame(self.login_window.tkinter)
        # self.frame.configure(background='grey')

        """Create a new vault"""
        ttk.Button(self.frame, text="create deposit", width=16, command=self.create_deposit).grid(row=1, column=1, padx=0, pady=10)

        """Deposit selection"""
        ttk.Button(self.frame, text="select deposit", width=16, command=self._button_select_deposit).grid(row=1, column=2)
        self.selected_deposit = ttk.Label(self.frame, text="deposit: not selected", foreground="black")
        self.selected_deposit.grid(row=1, column=3)

        """authentication by raw text token"""
        ttk.Label(self.frame, text="authorization key:", foreground="black").grid(row=2, column=1, padx=30, pady=10)
        self.key = ttk.Entry(self.frame, width=40, justify=self.login_window.TKINTER.CENTER, show='*')
        self.key.grid(row=2, column=2)
        ttk.Button(self.frame, text="authorize", width=12, command=self._button_raw_key_auth).grid(row=2, column=3, padx=30, pady=10)

        """authentication by browse filepath"""
        ttk.Label(self.frame, text="or", foreground="black").grid(row=5, column=2, pady=10)
        ttk.Label(self.frame, text="load .key file:", foreground="black").grid(row=6, column=1, pady=10)
        ttk.Button(self.frame, text="load", width=8, command=self._button_key_file_auth).grid(row=6, column=2)
        self.keyfile = ttk.Label(self.frame, text="key status: not loaded", foreground="black")
        self.keyfile.grid(row=6, column=3)

        """INSTRUCTIONS BUTTON"""
        ttk.Label(self.frame, text="Delivered by Trotylson | powered by Python3", foreground="#13a13a", font=('Arial', 8, 'bold')).grid(row=7, column=2, pady=70)
        ttk.Button(self.frame, text="how to use", command=Info()._show_instructions).grid(row=7, column=3)

        self.frame.pack()
        self.login_window.start()


    def scan_pendrive(self):
        self.pendrive_scan_thread.start_key_searching(self, self.deposit_key_name)

    def create_deposit(self):
        CREATOR = DepositCreator()
        CREATOR.start_window()
        print("DEPOSIT done")

    def _button_select_deposit(self):
        self.pendrive_scan_thread.job_done = False
        threading.Thread(target=self.start_procedure).start()
        # self.pendrive_scan_thread.job_done = True

    def start_procedure(self):
        my_deposit_path = filedialog.askopenfilename()
        if my_deposit_path.endswith(".vlt"):
            self.selected_deposit.destroy()
            self.selected_deposit = ttk.Label(self.frame, text="deposit: ON TABLE", foreground="green")
            self.selected_deposit.grid(row=1, column=3)
            self.deposit_path = my_deposit_path
            _re = re.findall(r"(.*)(/(.*))+(.vlt)$", my_deposit_path)
            if my_deposit_path:
                print("got deposit path")
                self.deposit_key_name = f"{_re[0][2]}.key"
                self.scan_pendrive()
        elif my_deposit_path == '':
            self.pendrive_scan_thread.job_done = True
        else:
            self.pendrive_scan_thread.job_done = True
            messagebox.showerror(".Vault bad file", "This is not a deposit box file for .Vault!")
        # print(deposit_box_path)
        self.pendrive_scan_thread.job_done = True

    def _button_key_file_auth(self):
        if self.deposit_path is None:
            messagebox.showinfo('Vault key', f"Before key loading select vault file!")
            return False
        
        my_key_path = filedialog.askopenfilename()

        if my_key_path:
            # Read and print the content (in bytes) of the file.
            # print(Path(key_path).read_bytes())
            self.keyfile.destroy()
            self.keyfile = ttk.Label(self.frame, text="key status: IN HAND", foreground="green")
            self.keyfile.grid(row=6, column=3)
            messagebox.showinfo('.Vault deposit key', f"Key loaded from:\n{my_key_path}\n\nNow I will try to open your .Vault deposit...")

            my_deposit = crypter.decode_base(deposit_path=self.deposit_path, key_path=my_key_path)

            if my_deposit:
                # print(vault)
                self.deposit_box = my_deposit['box']
                self.deposit_key = my_deposit['key']
                self.pendrive_scan_thread.job_done = True
                self.login_window.close()
                self.authorized = True
            else:
                self.key.delete(0, self.login_window.TKINTER.END)
                self.pendrive_scan_thread.job_done = True
        else:
            print("No file selected.")
            self.pendrive_scan_thread.job_done = True
        

    def _button_raw_key_auth(self):
        my_deposit = crypter.decode_base(deposit_path=self.deposit_path, key=self.key.get())

        if my_deposit:
            # print(vault)
            self.deposit_box = my_deposit['box']
            self.deposit_key = my_deposit['key']
            self.login_window.close()
            self.authorized = True
        else:
            self.key.delete(0, self.login_window.TKINTER.END)

    
    def __del__(self):
        self.pendrive_scan_thread.job_done = True
        print("Quitting")



