from libs.tk_builder import TkinterBuilder
from tkinter import ttk
import libs.crypter as crypter


class DepositCreator():
    def __init__(self):
        print("initializing")
        self.creator = TkinterBuilder(".Vault new deposit", "500x100")

        self.deposit_label = ttk.Label(self.creator.tkinter, text="deposit name: ", font=('bold'))
        self.deposit_id = ttk.Entry(self.creator.tkinter,  width=40, justify=self.creator.TKINTER.CENTER)
        self.deposit_button = ttk.Button(self.creator.tkinter, text="create", command=self.add_deposit)

        self.deposit_label.grid(row=1, column=1, padx= 15, pady=35)
        self.deposit_id.grid(row=1, column=2)
        self.deposit_button.grid(row=1, column=3)

        print("set items and starting window...")


    def start_window(self):
        self.creator.start()

    def add_deposit(self):
        new_depo_id = self.deposit_id.get()
        print(f"database {new_depo_id} created successfully")
        crypter.create_new_deposit(new_depo_id)
        self.creator.close()

    def close(self):
        self.creator.close()
        

