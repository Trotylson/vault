from libs.tk_builder import TkinterBuilder
from tkinter import *
from tkinter import ttk, messagebox
from libs.deposit import Deposit
import libs.crypter as Crypter
from info.informations import Info
from libs.menu_bar import MenuBar
from events.add_credentials_session import AddCredentials
import json


class VaultSession():
    def __init__(self, after_arg, deposit_path, deposit_key, deposit_content) -> None:
        """Main design parameters"""
        self.main_background = "#c9cbcb"
        self.child_frames_background = "white"

        """Vault session utils"""
        self.VAULT_WINDOW = TkinterBuilder(title=f".Vault ({deposit_content.get('deposit_id') if deposit_content else None} deposit unlocked)", window_size="730x600")
        self.VAULT_WINDOW.tkinter.config(pady=10, background=self.main_background)
        self.DEPOSIT = Deposit(deposit_path, deposit_key, deposit_content)
        self.INFO = Info()
        self.menu_bar = MenuBar(self)

        """WINDOW FRAME"""
        self.full_frame = Frame(self.VAULT_WINDOW.tkinter, background=self.main_background)
        self.full_frame.pack()

        """CREDENTIALS ROWS FRAME SETTING"""
        self.refresh_credentials_frame()

        """Check if .vlt file contains anythink and is it a right content"""
        self.deposit_content = self.check_deposit_content(self.DEPOSIT.deposit_box)

        """Set rows and columns"""
        # self.total_rows = len(self.deposit_content)
        # self.total_columns = len(self.deposit_content[0])

        """Prepare and initialize window"""
        self.VAULT_WINDOW.start_after(after_arg)


    def check_deposit_content(self, deposit_content):
        if deposit_content:
            try:
                return deposit_content['deposit_content']
            except KeyError:
                exit(10)
        exit(10)
        
    def create_credentials_table(self):
        for credentials in self.DEPOSIT.deposit_box['deposit_content']:
            button = ttk.Button(self.credential_frame, text=credentials['name'], command=lambda credentials=credentials: self.show_row_content(credentials))
            button.grid(padx=5, pady=5)

    def refresh_credentials_frame(self):
        try:
            self.credential_frame.destroy()
        except:
            pass
        
        self.credential_frame = Frame(self.full_frame, highlightthickness=1, background=self.child_frames_background)
        self.credential_frame.config(highlightbackground='black', highlightcolor='blue')
        self.credential_frame.pack(side=LEFT, padx=10, pady=10)
        self.create_credentials_table()
        
    def create_new_credentials(self):
        result = AddCredentials(self).create()
        if result:
            messagebox.showinfo("Done", "Changes were successfully applied.\nPlease save your deposit before quitting!")
            self.refresh_credentials_frame()

            
    def show_row_content(self, btn_row):
        # print(row['name'])
        try:
            self.row_content_frame.destroy()
        except:
            pass
        """ROW CONTENT FRAME SETTINGS"""
        self.row_content_frame = Frame(self.full_frame, highlightthickness=1, background=self.child_frames_background)
        self.row_content_frame.config(highlightbackground='black', highlightcolor='blue')
        self.row_content_frame.pack(side=RIGHT, padx=30, pady=10)
        
        ttk.Label(self.row_content_frame, text="name: ", background="white").grid(row=1, column=1, padx=10)
        name = ttk.Entry(self.row_content_frame)
        name.grid(row=1, column=2, padx=10, pady=5)
        name.insert(END, btn_row['name'])
        
        ttk.Label(self.row_content_frame, text="login: ", background="white").grid(row=2, column=1, padx=10)
        login = ttk.Entry(self.row_content_frame)
        login.grid(row=2, column=2, padx=10, pady=5)
        login.insert(END, btn_row['login'])
        
        ttk.Label(self.row_content_frame, text="password: ", background="white").grid(row=3, column=1, padx=10)
        password = ttk.Entry(self.row_content_frame)
        password.grid(row=3, column=2, padx=10, pady=5)
        password.insert(END, btn_row['password'])
        
        ttk.Label(self.row_content_frame, text="url: ", background="white").grid(row=4, column=1, padx=10)
        url = ttk.Entry(self.row_content_frame)
        url.grid(row=4, column=2, padx=10, pady=5)
        url.insert(END, btn_row['url'])
        
        ttk.Label(self.row_content_frame, text="description: ", background="white").grid(row=5, column=1, padx=10)
        description = ttk.Entry(self.row_content_frame, width=50)
        description.grid(row=5, column=2, padx=10, pady=5)
        description.insert(END, btn_row['description'])

        def accept_changes():
            print("Accepting changes")
            if name.get() != btn_row['name']:
                for deposit_content_row in self.deposit_content:
                    if deposit_content_row['name'] == name.get():
                        messagebox.showerror("Whait a minute...", f"You used the name {name.get()} with a different row!\nYou can not have two same named rows!\nPlease choose a different name.")
                        return False
            for _index in range(len(self.deposit_content)):
                if self.deposit_content[_index]['name'] == btn_row['name']:
                    self.deposit_content[_index]['name'] = name.get()
                    self.deposit_content[_index]['login'] = login.get()
                    self.deposit_content[_index]['password'] = password.get()
                    self.deposit_content[_index]['url'] = url.get()
                    self.deposit_content[_index]['description'] = description.get()
                    break
            self.event_save_deposit()
            self.refresh_credentials_frame()
            messagebox.showinfo("Changes applied.", "Changes were successfully applied.")

        
        def delete_credentials_row():
            ask = messagebox.askquestion(title="Remove credentials?", message="Are you sure you want to permanently delete this row?")
            if ask == "yes":
                for _index in range(len(self.deposit_content)):
                    if self.deposit_content[_index]['name'] == btn_row['name']:
                        del self.deposit_content[_index]
                        break
                self.event_save_deposit()
                self.refresh_credentials_frame()
                messagebox.showinfo("Removed", "Credentials removed,changes were successfully applied.")

        ttk.Button(self.row_content_frame, text="Remove", command=delete_credentials_row).grid(row=6, column=1, padx=10, pady=20)
        ttk.Button(self.row_content_frame, text="Accept changes", command=accept_changes).grid(row=6, column=2, padx=10, pady=20)
        ttk.Button(self.row_content_frame, text="Cancel", command=self.row_content_frame.destroy).grid(row=6, column=3, padx=10, pady=20)

    def event_save_deposit(self):
        Crypter.encrypt_deposit(self.DEPOSIT)
        print("File saved")
        # messagebox.showinfo("Deposit  saved", "Your deposit has been saved successfully!")

    def close(self):
        self.VAULT_WINDOW.close()
