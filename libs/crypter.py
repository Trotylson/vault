from cryptography.fernet import Fernet
import json
from tkinter import messagebox


def decode_base(deposit_path=None, key_path: str=None, key: str=None):
    if deposit_path:
        try:
            with open(f'{deposit_path}', 'r') as f:
                vault = f.read()
        except:
            print("Incorrected file selected.")
            messagebox.showerror(".Vault deposit key.", "Invalid .Vault deposit key!\nTry another one.")
            return False

    if key:
        try:
            fernet = Fernet(key)
            result = fernet.decrypt(bytes(vault, 'utf8'))
            result = json.loads(result)
        except ValueError:
            print("Invalid token")
            messagebox.showinfo('Unauthorized!', f'Your key/token is not correct!' )
            return False
    else:
        try:
            with open(f'{key_path}', 'r') as f:
                key = f.read()
            fernet = Fernet(key)
            result = fernet.decrypt(bytes(vault, 'utf8'))
            result = json.loads(result)

        except:
            print("Invalid token from file")
            messagebox.showinfo('Unauthorized!', f'Your key/token from file path is not correct!' )
            return False

    if result.get('deposit_locked'):
        return {"key": key, "box": result}
    else:
        messagebox.showinfo('Bad file!', f'This file is not a vault!' )
        return False
    
def encrypt_deposit(deposit):
    # print("saving depo", json.dumps(deposit.deposit_box, indent=4))
    fernet = Fernet(deposit.deposit_key)
    data = fernet.encrypt(bytes(json.dumps(deposit.deposit_box), 'utf8'))

    with open(f'{deposit.deposit_path}', 'wb') as f:
        f.write(data)


def create_new_deposit(deposit_id):
    from libs.deposit import Deposit

    vault = Deposit()
    key = Fernet.generate_key()

    vault.deposit_box['deposit_id'] = deposit_id

    with open(f'keyring/{deposit_id}.key', 'wb') as f:
        f.write(key)

    data = vault.deposit_box
    data = json.dumps(data)

    fernet = Fernet(key)
    data = fernet.encrypt(bytes(data, 'utf8'))

    with open(f'vault/{deposit_id}.vlt', 'wb') as f:
        f.write(data)

    return messagebox.showinfo('Created!', f'Deposit named "{deposit_id}" successfully created!\n\nYour {deposit_id}.vlt deposit file is stored in path: ./vault/{deposit_id}.vlt\nYour {deposit_id}.key file is stored in path: ./keyring/{deposit_id}.key' )