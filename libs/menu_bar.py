from tkinter import *
from info.informations import Info

class MenuBar():
    def __init__(self, VAULT):
        self.INFO = Info()
        self.vault_session = VAULT.VAULT_WINDOW.tkinter
        self.menubar = Menu(self.vault_session)
        self.vault_session.config(menu=self.menubar)

        self.file_menu = Menu(self.menubar)
        
        """ADD CASCADE OPTIONS"""
        self.file_menu.add_command(
            label='Add credentials',
            command=VAULT.create_new_credentials
        )

        self.file_menu.add_command(
            label="Save deposit",
            command=VAULT.event_save_deposit,
            underline=0
        )
        
        self.file_menu.add_command(
            label='Instructions',
            command=self.INFO._show_instructions
        )

        self.file_menu.add_command(
            label='Exit',
            command=VAULT.close
        )

        """ADD CASCADE FUNCTIONS"""
        self.menubar.add_cascade(
            label="File",
            menu=self.file_menu,
            underline=0
        )
        
        # self.menubar.add_cascade(
        #     label="Save deposit",
        #     command=VAULT.event_save_deposit,
        #     underline=0
        # )

        self.menubar.add_cascade(
            label='About',
            command=self.INFO._show_about,
            underline=0
        )