import tkinter
from tkinter import ttk


class TkinterBuilder():
    def __init__(self, title=None, window_size=None):
        self.TKINTER = tkinter
        # self.ttk = ttk
        self.tkinter = self.TKINTER.Tk()
        self.tkinter.title(title if title else "Your title here")
        self.tkinter.geometry(window_size if window_size else "300x200")
        # self.tkinter.configure(background='black')


    def start(self):
        return self.tkinter.mainloop()

    def close(self):
        return self.tkinter.destroy()
    
    def start_after(self, after_arg):
        """
        Call tkinter.Tk().after() function to start window 
        after the given argument
        """
        if after_arg:
            return self.tkinter.after(after_arg, self.tkinter.mainloop())
    
        