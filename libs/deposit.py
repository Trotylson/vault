
class Deposit():
    def __init__(self, depo_path=None, depo_key=None, depo_box=None) -> None:
        self.deposit_key = depo_key
        self.deposit_path = depo_path
        self.deposit_box = depo_box if depo_box else {
            "deposit_id": str,
            "deposit_locked": True,
            "deposit_content": [
                {
                    "name": "Joe Doe",
                    "login": "root@192.168.1.100",
                    "password": "my_secret_password",
                    "url": "http://",
                    "description": "This is an example of the .Vault deposit box credentials."
                }
            ]
        }

    
    def insert_to_deposit(self, content):
        self.deposit_box['deposit_content'].append(content)
        