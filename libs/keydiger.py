import os, string
import string
from ctypes import windll


class KeyDiger():
    def __init__(self):
        self.auth_obj = None
        self.correct_pendrive_plugged = False
        self.key_file_path = None
        self.key_file_name = None
        self.job_done = False

        self.drives_before = self.get_drives()
        # self.start_key_searching()


    def get_drives(self):
        """Scan discs / drives on the system"""
        drives = []
        bitmask = windll.kernel32.GetLogicalDrives()
        for letter in string.ascii_uppercase:
            if bitmask & 1:
                drives.append(letter)
            bitmask >>= 1

        return drives

    def search_key_file(self, drive):
        for subdir, dirs, files in os.walk(f'{drive}:'):
            for file in files:
                if file == self.key_file_name:
                    print(file)
                    return f"{subdir}/{file}" 
                
    def set_key_value(self):
        with open(self.key_file_path, 'r') as f:
            key = f.read()
        self.auth_obj.deposit_key = key
        self.auth_obj.key.insert(0, key)

    def start_key_searching(self, auth_obj, key_name):
        self.auth_obj = auth_obj
        self.key_file_name = key_name
        while self.correct_pendrive_plugged is False and self.job_done is False:
            drives_new_result = self.get_drives()
            if len(self.drives_before) < len(drives_new_result):
                print(drives_new_result[-1])
                self.key_file_path = self.search_key_file(drives_new_result[-1])
                if self.key_file_path:
                    self.correct_pendrive_plugged = True
                    self.set_key_value()
                else:
                    self.drives_before = self.get_drives()
            elif len(self.drives_before) > len(drives_new_result):
                self.drives_before = self.get_drives()

    
    def __del__(self):
        self.job_done = True


