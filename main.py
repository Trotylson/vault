from events.authorization_session import authorization_session
from events.vault_session import VaultSession
from libs.keydiger import KeyDiger

pen_scan_thread = KeyDiger()


def main():
    authorization = authorization_session(pen_scan_thread)
    VaultSession(authorization.authorized, authorization.deposit_path, authorization.deposit_key, authorization.deposit_box)

if __name__ == '__main__':
    main()
    pen_scan_thread.job_done = True

