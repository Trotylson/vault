# .Vault Technical Documentation

.Vault is a Python program that utilizes the tkinter and cryptography libraries for user interface and encryption/decryption operations.

## Table of Contents

1. [Introduction](#introduction)
2. [Installation](#installation)
3. [Usage](#usage)
4. [File Structure](#file-structure)
5. [Modules and Classes](#modules-and-classes)
6. [Contributing](#contributing)
7. [License](#license)
8. [Author](#author)

## Introduction <a name="introduction"></a>

The `.Vault` program is designed to provide users with a secure solution for creating, reading, and modifying password vaults containing authentication data. By utilizing the tkinter library, it offers a user-friendly interface for easy interaction. The cryptography library is used to ensure the security of the password vaults through encryption and decryption processes. The program also supports automatic authentication using a key file located on a USB drive, providing an additional layer of convenience and security.

## Installation <a name="installation"></a>

To install and run the `.Vault` program, follow these steps:

1. Ensure that Python 3.x is installed on your system.
2. Download or clone the `.Vault` source code from the repository.
3. Open a terminal or command prompt and navigate to the directory where the source code is located.
4. Install the required dependencies by running the following command: `pip install -r requirements.txt`
5. Once the dependencies are installed, you can run the program using the following command: `python3 main.py`

## Usage <a name="usage"></a>

The `.Vault` program provides various features for managing password vaults. Here are the main functionalities:

1. Creating a Password Vault: Use the program to create a new password vault by specifying a name for the vault. The program will generate a new vault file, encrypt it, and generate a unique decryption key.

2. Logging into an Existing Password Vault: Access an existing password vault by loading the vault file and providing the access key. The program will authenticate the user and unlock the password vault for further operations.

3. Modifying a Password Vault: Once logged in, users can add, modify, or delete credentials stored in the password vault. The program ensures the security and integrity of the vault through encryption and decryption processes.

4. Automatic Authentication: The program supports automatic authentication using a key file located on a USB drive. When a vault file is selected, the program automatically searches for the corresponding key file on the connected USB drive, simplifying the authentication process.

For detailed instructions on using these features, refer to the user documentation.

## File Structure <a name="file-structure"></a>

The `.Vault` program consists of the following files and directories:

- `main.py`: The main entry point of the program that initializes the user interface.
- `events/authorization_session.py`: Module responsible for managing the authorization session and user authentication.
- `events/vault_session.py`: Module that handles the session of a password vault, including adding, modifying, and deleting credentials.
- `libs/keydiger.py`: Module that provides the functionality to search for a key file on a connected USB drive.
- `events/add_credentials_session.py`: Module responsible for managing the session of adding credentials to a password vault.
- `events/deposit_creator.py`: Module that handles the creation of a new password vault.
- `libs/crypter.py`: Module that contains functions for encrypting and decrypting the password vault.
- `libs/deposit.py`: Module that defines the `Deposit` class for interacting with a password vault.
- `libs/tk_builder.py`: Module that contains the `TkinterBuilder` class for building the user interface using tkinter.
- `libs/menu_bar.py`: Module that provides the `MenuBar` class for creating the menu bar in the user interface.

## Modules and Classes <a name="modules-and-classes"></a>

### `main.py`

- `main()`: The main function that initializes the user interface and starts the program.

### `events/authorization_session.py`

- `AuthorizationSession`: Class responsible for managing the authorization session and user authentication.

### `events/vault_session.py`

- `VaultSession`: Class that handles the session of a password vault, including adding, modifying, and deleting credentials.

### `libs/keydiger.py`

- `Keydiger`: Class that provides the functionality to search for a key file on a connected USB drive.

### `events/add_credentials_session.py`

- `AddCredentialsSession`: Class responsible for managing the session of adding credentials to a password vault.

### `events/deposit_creator.py`

- `DepositCreator`: Class that handles the creation of a new password vault.

### `libs/crypter.py`

- `Crypter`: Class that contains functions for encrypting and decrypting the password vault.

### `libs/deposit.py`

- `Deposit`: Class that represents a password vault and provides methods for interacting with the vault.

### `libs/tk_builder.py`

- `TkinterBuilder`: Class that facilitates the building of the user interface using the tkinter library.

### `libs/menu_bar.py`

- `MenuBar`: Class that provides methods for creating the menu bar in the user interface.

## Contributing <a name="contributing"></a>

Contributions to the `.Vault` program are welcome. If you encounter any issues, have suggestions, or would like to contribute code, please feel free to submit a pull request.

## License <a name="license"></a>

The `.Vault` program is released under the [MIT License](https://opensource.org/licenses/MIT).

## Author <a name="author"></a>

The `.Vault` program was created by `Trotylson`.
